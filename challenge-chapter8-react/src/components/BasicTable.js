import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
// import { pallete } from '@material-ui/system';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const BasicTable = (props) => {
  return (
    <TableContainer component={Paper} >
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Username</StyledTableCell>
            <StyledTableCell >email</StyledTableCell>
            <StyledTableCell >experience</StyledTableCell>
            <StyledTableCell >level</StyledTableCell>
            <StyledTableCell ></StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        
          {props.players.map((player) => {
            const {id, username, email, experience, lvl} = player;
            return (
            <TableRow key={id}>
              <TableCell component="th" scope="row">
                {username}
              </TableCell>
              <TableCell >{email}</TableCell>
              <TableCell >{experience}</TableCell>
              <TableCell >{lvl}</TableCell>
              <TableCell>
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                onClick={()=> props.editPlayer(player)}
                >
                    Update
              </Button>
              </TableCell>
            </TableRow>
          )})}
        </TableBody>
      </Table>
    </TableContainer>
  );
}


export default BasicTable;