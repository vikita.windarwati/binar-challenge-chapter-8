
import React, { useState, useEffect }  from 'react';
import { Button, TextField, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// import BasicTable from './BasicTable';

const useStyles = makeStyles((theme)=> ({
    textField: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    table: {
        minWidth: 650,
    },
}));


const UpdateData = (props) => {

    const [player, setPlayer] = useState(props.currentUser);
    console.log(props.currentUser);    
    useEffect(()=> {
        setPlayer(props.currentUser)
    }, [props])

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPlayer({...player, [name]:value})
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        props.updatePlayer(player);
    }
    
    const classes = useStyles();

    return (
        <Container maxWidth="md">
            <h1>Update</h1>
                <form className={classes.textField} autoComplete="off" onSubmit={handleSubmit}>
                    <div>
                    <TextField required label="username" htmlFor="username"
                        id="username"
                        name="username"
                        type="text"
                        onChange={handleChange}
                        value={player.username}
                    />
                    <TextField required label="e-mail"
                        id="email"
                        name="email"
                        type="email"
                        onChange={handleChange}
                        value={player.email}
                    />
                    <TextField required label="password"
                        id="password"
                        name="password"
                        type="password"
                        onChange={handleChange}
                        value={player.password}
                    />
                        <Button 
                            type="submit"
                            variant="contained"
                            color="primary"
                            // onClick={() => props.setUpdate(false)}
                            >Update</Button>
                    </div>
                </form>
                <br></br>
        </Container>
    );
};

export default UpdateData;