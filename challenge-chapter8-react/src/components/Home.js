// import React, { useState } from 'react';
// import { Container } from '@material-ui/core';
// import BasicTable from './BasicTable';
// import FormRegister from './FormRegister'
// import UpdateData from './UpdateData';
// import dummyData from './dummyData';


// const Home = () => {
//     const [players, setPlayers] = useState(dummyData);
    
//     const [updating, setUpdating] = useState(false);

//     const initPlayer = { id: null, username:'', email:'', password:'', experience:0, lvl:0,};

//     const [currentUser, setCurrentUser] = useState(initPlayer);

//     const register = player => {
//         player.id = new Date().getTime().toString();
//         setPlayers([...players, player])
//     }

//     const editPlayer = (player) => {
//         setUpdating(true);
//         setCurrentUser(player);
//         // console.log(player);
//     }

//     const updatePlayer = (editedPlayer) => {
//         setPlayers(players.map(player => (player.id === currentUser.id ? editedPlayer : player)))
//     }

//     return(
//         <Container maxWidth="md">
//             <h1>Home</h1>
//             <br></br>
//             <h1>View Players</h1>
//             <br></br>
//             <BasicTable players={players} editPlayer={editPlayer} />
//             <br></br>
//             <br></br>
//             {
//                 updating ? (
//                     <div>
//                         <UpdateData 
//                             currentUser={currentUser}
//                             setUpdating={setUpdating}
//                             updatePlayer={updatePlayer}
//                         />
//                     </div>
//                 ) : (
//                     <div>
//                         <FormRegister register={register} />
//                     </div>
//                 )
//             }
//         </Container>
//     ) 
// }

// export default Home;