import React, { useState }  from 'react';
import { Button, TextField, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme)=> ({
    textField: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    table: {
        minWidth: 650,
    },
}));


const FormRegister = (props) => {
    const [player, setPlayer] = useState({
        username:'', email:'', password:'',
        experience:0,
        lvl:0,
    })
    
    const handleChange = (e) => {
        const { name, value } = e.target;
        setPlayer({...player, [name]:value})
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        handleChange(e, props.register(player));
        // console.log(player)
        setPlayer({username:'', email:'', password:'', experience:0, lvl:0})
    }
    
    const classes = useStyles();

    return (
        <Container maxWidth="md">
            <h1>Register</h1>
                <form className={classes.textField} autoComplete="off" onSubmit={handleSubmit}>
                    <div>
                    <TextField required label="username" htmlFor="username"
                        // variant="outlined"
                        id="username"
                        name="username"
                        type="text"
                        onChange={handleChange}
                        value={player.username}
                    />
                    <TextField required label="e-mail"
                        // variant="outlined"
                        id="email"
                        name="email"
                        type="email"
                        onChange={handleChange}
                        value={player.email}
                    />
                    <TextField required label="password"
                        id="password"
                        name="password"
                        type="password"
                        onChange={handleChange}
                        value={player.password}
                    />
                        <Button type="submit" variant="contained" color="primary">Submit</Button>
                    </div>
                </form>
                <br></br>
        </Container>
    );
};

export default FormRegister;