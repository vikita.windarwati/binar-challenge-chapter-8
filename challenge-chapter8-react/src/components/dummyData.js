const dummyData = [
    {
        id: 59348593485,
        username: 'junjiIto',
        email: 'AmigaraFault@mail.com',
        password: '*******',
        experience: 0,
        lvl: 0,
    },
    {
        id: 53458345000,
        username: 'Impostor',
        email: 'amongus@mail.com',
        password: '*******',
        experience: 0,
        lvl: 0,
    }
];

export default dummyData;